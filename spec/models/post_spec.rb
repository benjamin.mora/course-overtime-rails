require 'rails_helper'

# RSpec.describe Post, type: :model do
#   describe "Creation" do
    # TODO: error user must exist
    
    
    # before do
    #   @post = Post.create(date: Date.today, rationale: "Anything")
    # end

    # it "can be created" do
    #   expect(@post).to be_valid  
    # end

    # it "cannot be created without a date and rationale" do
    #   @post.date = nil
    #   @post.rationale = nil
    #   expect(@post).to_not be_valid
    # end
#   end
  
# end


RSpec.describe Post, type: :model do
  describe "Creation" do
    before do
      user = User.create(
      email: "benja@gmail.com",
      password: "123123",
      first_name: "Bem",
      last_name: "torres")
  		@post = Post.create(date: Date.today, rationale: "Anything", user_id: user.id)
  	end

  	it 'can be created' do	
  		expect(@post).to be_valid
  	end

  	it 'cannot be created without a date and rationale' do
  		@post.date = nil
  		@post.rationale = nil
  		expect(@post).to_not be_valid
  	end
  end
end
