require 'rails_helper'

RSpec.describe User, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
  describe "Creation" do
    before do
      @user = User.create(
        email: "benja@gmail.com",
        password: "123123",
        first_name: "Bem",
        last_name: "torres")
    end

    it "can be created" do
      expect(@user).to be_valid  
    end

    it "cannot be created without first_name, last_name" do
      @user.first_name = nil
      @user.last_name = nil
      expect(@user).to_not be_valid 
    end

  end
end
